﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace exit_website.Data
{
    public class Ticket
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public ApplicationUser Reporter { get; set; }
        public string Description { get; set; }
        public TicketPriority Priority { get; set; }
        [Display(Name="Assigned To")]
        public ApplicationUser? AssignedTo { get; set; }
        public DateTime Created { get; set; }
        public DateTime Closed { get; set; }
        public virtual ICollection<TicketUpdate> TicketUpdates { get; } = new List<TicketUpdate>();

        public enum TicketPriority
        {
            Unprioritized = 0,
            Low = 1,
            Normal = 2,
            High = 3,
            Critical = 4
        }
    }
}
