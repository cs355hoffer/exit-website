﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace exit_website.Data.Migrations
{
    public partial class TicketUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TicketUpdates",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TicketID = table.Column<int>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    Time = table.Column<double>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TicketUpdates", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TicketUpdates_Tickets_TicketID",
                        column: x => x.TicketID,
                        principalTable: "Tickets",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketUpdates_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TicketUpdates_TicketID",
                table: "TicketUpdates",
                column: "TicketID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketUpdates_UserId",
                table: "TicketUpdates",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TicketUpdates");
        }
    }
}
