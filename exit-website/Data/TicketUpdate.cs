﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace exit_website.Data
{
    public class TicketUpdate
    {
        public int ID { get; set; }
        public Ticket Ticket { get; set; }
        public string Comment { get; set; }
        public double Time { get; set; }
        public ApplicationUser User { get; set; }
        public DateTime TimeAdded { get; set; }
    }
}
