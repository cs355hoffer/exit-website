﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace exit_website.Data
{
    public class ApplicationUser : IdentityUser
    {
        //Allow the UserType field to be null.  This avoids errors in early development.
        public ApplicationUserType? UserType { get; set; }

        public enum ApplicationUserType {
            ReadOnly = 0,
            Admin = 1,
            Dev = 2
        }
    }
}
