﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using exit_website.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace exit_website.Pages.Tickets
{
    [Authorize] //Forcing User to be logged in
    public class EditModel : PageModel
    {
        private readonly exit_website.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public EditModel(exit_website.Data.ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [BindProperty]
        public Ticket Ticket { get; set; }
        [BindProperty]
        public string AssignedId { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == _userManager.GetUserId(HttpContext.User));
            if (currentUser.UserType == ApplicationUser.ApplicationUserType.ReadOnly)
            {
                //If the user has read only access, just transfer them to the read only view of the ticket
                return new RedirectToPageResult("Details", new { id });
            }

            if (id == null)
            {
                return NotFound();
            }

            Ticket = await _context.Tickets.FirstOrDefaultAsync(m => m.ID == id);
            if (Ticket.AssignedTo == null)
            {
                AssignedId = "";
            }
            else
            {
                AssignedId = Ticket.AssignedTo.Id;
            }

            //Get User Dropdown values
            List<ApplicationUser> applicationUsers = _context.Users.ToList();
            ViewData["users"] = new SelectList(applicationUsers, "Id", "UserName");

            if (Ticket == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Ticket.AssignedTo = _context.Users.Where(u => u.Id == AssignedId).FirstOrDefault();

            //Check if close or reopen buttons were pressed.  Update ticket accordingly
            if (Request.Form["Close"].Count() > 0)
            {
                Ticket.Closed = DateTime.Now;
            }
            else if (Request.Form["Reopen"].Count() > 0)
            {
                Ticket.Closed = DateTime.MaxValue;
            }

            if (Ticket.Closed.Year == 9999) //Handle case where editing other fields is rounding this value to just below max
            {
                Ticket.Closed = DateTime.MaxValue;
            }

            _context.Entry(Ticket).Reference("AssignedTo").IsModified = true;  //Added Field Explicitly.  Wasn't detecting changes when setting to null.
            _context.Attach(Ticket).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TicketExists(Ticket.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            //Redirect to ticket after saving
            return new RedirectToPageResult("Details", new { Ticket.ID });
        }

        private bool TicketExists(int id)
        {
            return _context.Tickets.Any(e => e.ID == id);
        }
    }
}
