﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using exit_website.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;

namespace exit_website.Pages.Tickets
{
    public class IndexModel : PageModel
    {
        private readonly exit_website.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public IndexModel(exit_website.Data.ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IList<Ticket> Ticket { get;set; }

        [BindProperty]
        public int Open { get; set; }
        [BindProperty]
        public string SearchFilter { get; set; }
        [BindProperty]
        public string AssignedId { get; set; }



        public async Task<IActionResult> OnGetAsync(int? open, string? searchFilter, string? assignedID)
        {
            //If the user has dev access only, just transfer them to their assigned tickets
            ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == _userManager.GetUserId(HttpContext.User));
            if (currentUser.UserType == ApplicationUser.ApplicationUserType.ReadOnly)
            {
                return new RedirectToPageResult("Assigned");
            }

            //Check for nulls from URL Parameters
            int openParameter = open ?? 1;
            string searchParameter = searchFilter ?? "";
            string assignedIDParameter = assignedID ?? "";

            //Update Form Values Binded on the CSHTML page 
            Open = openParameter;
            SearchFilter = searchParameter;
            AssignedId = assignedIDParameter;

            //Populate user dropdown values
            var applicationUsers = _context.Users.ToList();
            ViewData["users"] = new SelectList(applicationUsers, "Id", "UserName");

            //Tweak Incoming Values for Database Search
            searchParameter = searchParameter.ToUpper();  //Our search isn't case sensitive on text
            assignedIDParameter = assignedIDParameter.ToUpper();  //Our search isn't case sensitive on user

            //Note: This can be slow for large datasets.  Written for clarity, not speed.
            DbSet<Ticket> AllTickets = _context.Tickets;

            //Show only tickets that contain our search
            IQueryable<Ticket> FilteredTickets = AllTickets.Where(t => t.Title.ToUpper().Contains(searchParameter) || t.Description.ToUpper().Contains(searchParameter));

            //Show only tickets that are open/closed/all
            if (openParameter == 1)
            {
                FilteredTickets = FilteredTickets.Where(t => t.Closed == DateTime.MaxValue);  // If date value is max value, we'll assume the ticket is still open
            }
            else if (openParameter == 0)
            {
                FilteredTickets = FilteredTickets.Where(t => t.Closed < DateTime.MaxValue);
            }

            //Show only tickets for the specified user
            if (assignedIDParameter != "")
            {
                if (assignedIDParameter == "NULL")
                {
                    FilteredTickets = FilteredTickets.Where(t => t.AssignedTo.Equals(null));
                }
                else
                {
                    FilteredTickets = FilteredTickets.Where(t => t.AssignedTo.Id.ToUpper() == assignedIDParameter);
                }
            }

            //Show newest tickets first
            FilteredTickets = FilteredTickets.OrderByDescending(t => t.ID);

            //Return filtered results to page
            Ticket = await FilteredTickets.ToListAsync();

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await Task.Yield(); //Since we're not using async, I'm putting this here to remove warning

            return RedirectToPage("./Index", new { open = Open, searchFilter = SearchFilter, assignedID = AssignedId });
        }

        public double HoursLogged(Ticket t)
        {
            //Getting Ticket updates.
            ICollection<TicketUpdate> TicketUpdates = _context.TicketUpdates.Where(m => m.Ticket.ID == t.ID).OrderByDescending(m => m.ID).ToList();

            //Getting sum of hours logged
            double hoursLogged = 0.0;

            foreach (var update in TicketUpdates)
            {
                hoursLogged += update.Time;
            }

            return hoursLogged;
        }
    }
}
