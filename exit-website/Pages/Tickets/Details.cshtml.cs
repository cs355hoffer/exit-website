﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using exit_website.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel;

namespace exit_website.Pages.Tickets
{
    [Authorize]
    public class DetailsModel : PageModel
    {
        private readonly exit_website.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public DetailsModel(exit_website.Data.ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [BindProperty]
        public Ticket Ticket { get; set; }
        [BindProperty]
        public TicketUpdate TicketUpdate { get; set; }
        [BindProperty]
        public bool ShouldCloseTicket { get; set; }
        public bool CanEdit { get; set; } = false;
        public IList<TicketUpdate> TicketUpdates { get; set; }

        [BindProperty]
        public string Comment { get; set; }
        [BindProperty]
        public double TimeWorked { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == _userManager.GetUserId(HttpContext.User));
            if (currentUser.UserType == ApplicationUser.ApplicationUserType.Admin || currentUser.UserType == ApplicationUser.ApplicationUserType.Dev)
            {
                CanEdit = true;
            }

            if (id == null)
            {
                return NotFound();
            }

            Ticket = await _context.Tickets.FirstOrDefaultAsync(m => m.ID == id);
            TicketUpdates = await _context.TicketUpdates.Where(m => m.Ticket.ID == id).OrderByDescending(m => m.ID).ToListAsync();

            if (Ticket == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            //If the user doesn't have access, redirect to main page
            ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == _userManager.GetUserId(HttpContext.User));
            if (currentUser.UserType == ApplicationUser.ApplicationUserType.ReadOnly)
            {
                return new RedirectToPageResult("Details", new { Ticket.ID });
            }

            TicketUpdate.TimeAdded = DateTime.Now;
            TicketUpdate.Ticket = _context.Tickets.FirstOrDefault(x => x.ID == Ticket.ID);
            TicketUpdate.User = currentUser;

            if (ShouldCloseTicket)
            {
                TicketUpdate.Ticket.Closed = DateTime.Now;
            }
            
            _context.TicketUpdates.Add(TicketUpdate);
            await _context.SaveChangesAsync();

            return new RedirectToPageResult("Details", new { Ticket.ID });
        }
    }
}
