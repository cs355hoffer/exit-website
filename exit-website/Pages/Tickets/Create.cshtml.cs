﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using exit_website.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace exit_website.Pages.Tickets
{
    //Force user to be logged in to create a ticket
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly exit_website.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        //Adding the user manager here is Injecting the dependency.
        public CreateModel(exit_website.Data.ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult OnGet()
        {
            //Handle getting the current user.  If the user is not Admin or Dev, redirect to home page.
            ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == _userManager.GetUserId(HttpContext.User));
            if (currentUser.UserType == ApplicationUser.ApplicationUserType.ReadOnly)
            {
                return new RedirectToPageResult("/Index");
            }

            var applicationUsers = _context.Users.ToList();

            ViewData["users"] = new SelectList(applicationUsers, "Id", "UserName");
            
            return Page();
        }

        [BindProperty]
        public Ticket Ticket { get; set; }
        [BindProperty]
        public string AssignedId { get; set; }
        

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            //Set the reporter to use the current user automatically
            ApplicationUser user = await _userManager.GetUserAsync(User);
            Ticket.Reporter = user;

            //Lookup real user id from string value
            Ticket.AssignedTo = await _context.Users.Where(u => u.Id == AssignedId).FirstOrDefaultAsync();
            
            Ticket.Created = DateTime.Now;
            Ticket.Closed = DateTime.MaxValue;

            _context.Tickets.Add(Ticket);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
