﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using exit_website.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace exit_website.Pages.Tickets
{
    [Authorize]
    public class AssignedModel : PageModel
    {
        private readonly exit_website.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public AssignedModel(exit_website.Data.ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IList<Ticket> Ticket { get;set; }

             public async Task<IActionResult> OnGetAsync()
        {
            //If the user has dev access only, just transfer them to their assigned tickets
            string currentUserId = _userManager.GetUserId(HttpContext.User);
            //ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == currentUserId);
            //if (currentUser.UserType == ApplicationUser.ApplicationUserType.ReadOnly)
            //{
            //    //I decided not to redirect for now.  Might replace later.
            //    //return new RedirectToPageResult("Assigned");
            //}

            //Populate user dropdown values
            var applicationUsers = _context.Users.ToList();
            ViewData["users"] = new SelectList(applicationUsers, "Id", "UserName");

            //Note: This can be slow for large datasets.  Written for clarity, not speed.
            DbSet<Ticket> AllTickets = _context.Tickets;

            //Show only open tickets
            IQueryable<Ticket> FilteredTickets = AllTickets.Where(t => t.Closed == DateTime.MaxValue);

            //FilteredTickets = FilteredTickets.Where(t => t.AssignedTo.Id.ToUpper() == currentUserId.ToUpper());
            
            FilteredTickets = FilteredTickets.OrderByDescending(t => t.ID);

            //Return filtered results to page
            Ticket = await FilteredTickets.ToListAsync();

            return Page();
        }

        public double HoursLogged(ICollection<TicketUpdate> updates)
        {
            double hoursLogged = 0.0;

            foreach (var update in updates)
            {
                hoursLogged += update.Time;
            }

            return hoursLogged;
        }
    }
}
