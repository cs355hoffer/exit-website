﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using exit_website.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace exit_website.Pages.Users
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly exit_website.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;


        // Note: I'm not in love with how I've handled users in this project.
        // If I had more time on the project, I'd consider creating more robust user pages.  
        // I'd also consider using the built in roles instead of a user type field.
        public IndexModel(exit_website.Data.ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IList<ApplicationUser> UserList { get;set; }

        public async Task<IActionResult> OnGetAsync()
        {
            ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == _userManager.GetUserId(HttpContext.User));
            if (currentUser.UserType == ApplicationUser.ApplicationUserType.ReadOnly)
            {
                return new RedirectToPageResult("/Index");
            }

            UserList = await _context.Users.ToListAsync();

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == _userManager.GetUserId(HttpContext.User));
            if (currentUser.UserType == ApplicationUser.ApplicationUserType.ReadOnly)
            {
                return new RedirectToPageResult("/Index");
            }

            UserList = await _context.Users.ToListAsync();

            return Page();
        }
    }
}
