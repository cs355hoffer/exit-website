﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using exit_website.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace exit_website.Pages.Users
{
    [Authorize]
    public class DetailsModel : PageModel
    {
        private readonly exit_website.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public DetailsModel(exit_website.Data.ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public ApplicationUser ApplicationUser { get; set; }

        public async Task<IActionResult> OnGetAsync(string? id)
        {
            ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == _userManager.GetUserId(HttpContext.User));
            if (currentUser.UserType == ApplicationUser.ApplicationUserType.ReadOnly)
            {
                return new RedirectToPageResult("/Index");
            }

            if (id == null)
            {
                return NotFound();
            }

            ApplicationUser = await _context.Users.FirstOrDefaultAsync(m => m.Id == id);

            if (ApplicationUser == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string? id)
        {
            ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == _userManager.GetUserId(HttpContext.User));
            if (currentUser.UserType != ApplicationUser.ApplicationUserType.Admin)
            {
                return NotFound();
            }
            if (id == null)
            {
                return NotFound();
            }

            ApplicationUser = await _context.Users.FirstOrDefaultAsync(m => m.Id == id);

            if (ApplicationUser == null)
            {
                return NotFound();
            }

            if (Request.Form["Admin"].Count() > 0)
            {
                ApplicationUser.UserType = ApplicationUser.ApplicationUserType.Admin;
            }
            else if (Request.Form["Dev"].Count() > 0)
            {
                ApplicationUser.UserType = ApplicationUser.ApplicationUserType.Dev;
            }
            else if (Request.Form["ReadOnly"].Count() > 0)
            {
                ApplicationUser.UserType = ApplicationUser.ApplicationUserType.ReadOnly;
            }

            _context.Attach(ApplicationUser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return Page();
        }

    }
}
